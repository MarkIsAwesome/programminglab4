import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class JunitSubtractonacciSequence {

    @Test
    public void test01() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        int result = sub.getTerm(3);
        assertEquals(-2, result);
    }

    @Test
    public void test02() {
        SubtractonacciSequence sub = new SubtractonacciSequence(-2, 6);
        int result = sub.getTerm(5);
        assertEquals(-22, result);
    }

    @Test
    public void test03() {
        SubtractonacciSequence sub = new SubtractonacciSequence(4, -2);
        int result = sub.getTerm(7);
        assertEquals(36, result);
    }
}
