public class SubtractonacciSequence extends Sequence {
    private int firstNum;
    private int secondNum;

    public SubtractonacciSequence(int firstNum, int secondNum) {
        this.firstNum = firstNum;
        this.secondNum = secondNum;
    }

    public int getTerm(int position) {
        int preNum = this.firstNum;
        int nextNum = this.secondNum;
        int resNum = 0;

        if (position == 1) {
            return this.firstNum;
        } else if (position == 2) {
            return this.secondNum;
        } else {
            for (int i = 2; i < position; i++) {
                resNum = preNum - nextNum;
                preNum = nextNum;
                nextNum = resNum;
            }
            return resNum;
        }
    }
}
