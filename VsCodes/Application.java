import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        Boolean done = false;
        Scanner input = new Scanner(System.in);

        while (done != true) {
            try {

                System.out.println(
                        "Please put a sequence of fib and sub with the 2 sequence number and an 'x' in the end. Seperate them using ';'. Ex:Fib;1;1;x;Sub;4;2;x;");
                String userSeq = input.next();
                Sequence[] sequence = parse(userSeq);
                for (int i = 0; i < sequence.length; i++) {
                    for (int j = 1; j <= 10; j++) {
                        System.out.print(sequence[i].getTerm(j) + ", ");
                    }
                    System.out.println("\n");
                }
                done = true;
            } catch (Exception e) {
                System.out
                        .println("ERROR: NOT A SEQUENCE! The requested String has not been entered. Check the sample.");
                done = false;
            }
        }

    }

    // print method
    public static void print(Sequence[] sequence, int n) {
        for (int i = 0; i < sequence.length; i++) {
            System.out.println(sequence[i].getTerm(n));
        }
    }

    // parse method
    public static Sequence[] parse(String infos) {
        String[] sq = infos.split(";");
        Sequence[] sequence = new Sequence[sq.length / 4];
        int firstNum = 0;
        int secNum = 0;
        int countIndex = 0;
        for (int i = 0; i < sq.length; i = i + 4) {
            firstNum = Integer.parseInt(sq[i + 1]);
            secNum = Integer.parseInt(sq[i + 2]);
            if (sq[i].equals("Fib")) {
                sequence[countIndex] = new FibonacciSequence(firstNum, secNum);
                countIndex++;
            } else if (sq[i].equals("Sub")) {
                sequence[countIndex] = new SubtractonacciSequence(firstNum, secNum);
                countIndex++;
            }
        }
        return sequence;
    }

}