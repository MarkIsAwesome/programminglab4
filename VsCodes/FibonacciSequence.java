/**
 * FibonacciSequence
 */
public class FibonacciSequence extends Sequence {

    private int firstDig;
    private int secondDig;

    public FibonacciSequence(int firstDig, int secondDig) {
        this.firstDig = firstDig;
        this.secondDig = secondDig;
    }

    public int getFirstDig() {
        return this.firstDig;
    }

    public int getfSecondDig() {
        return this.secondDig;
    }

    public int getTerm(int position) {

        int prevNum = this.firstDig;
        int currNum = this.secondDig;
        int nextNum = 0;

        if (position == 1) {
            return this.firstDig;
        } else if (position == 2) {
            return this.secondDig;
        } else {
            for (int i = 2; i < position; i++) {

                nextNum = prevNum + currNum;
                prevNum = currNum;
                currNum = nextNum;

            }
            return nextNum;
        }

    }
}