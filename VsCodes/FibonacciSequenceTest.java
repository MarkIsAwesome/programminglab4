import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class FibonacciSequenceTest {
    @Test
    public void test1() {
        FibonacciSequence add = new FibonacciSequence(2, 4);
        assertEquals(2, add.getFirstDig());
    }

    @Test
    public void test2() {
        FibonacciSequence add = new FibonacciSequence(2, 4);
        assertEquals(4, add.getfSecondDig());
    }

    @Test
    public void test3() {
        FibonacciSequence add = new FibonacciSequence(2, 4);
        assertEquals(6, add.getTerm(3));
    }

    @Test
    public void test4() {
        FibonacciSequence add = new FibonacciSequence(2, 4);
        assertEquals(26, add.getTerm(6));
    }

    @Test
    public void test5() {
        FibonacciSequence add = new FibonacciSequence(2, 4);
        assertEquals(110, add.getTerm(9));
    }
}
